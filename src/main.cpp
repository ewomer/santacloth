#include <iostream>
#include <vector>
#include <memory>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <array>

using std::cerr;
using std::cout;
using std::exception;
using std::make_unique;
using std::runtime_error;
using std::string;
using std::stringstream;
using std::unique_ptr;
using std::vector;

#include "Vec2.h"
#include "Vec3.h"

/**
 * @class Data
 * @author Ewomer
 * @date 30/12/18
 * @file main.cpp
 * @brief
 */
struct Data {
  string    id;
  Vec2<int> pos;
  Vec2<int> area;
  Vec3      color;
};

/**
 * @brief Write a ppm formated string to disk as a ppm image
 * @param filename
 * @param contents
 * @return
 */
int writeFilePPM(std::string const& filename, std::string const& contents)
{
  std::ofstream ofile;
  {
    // Close it in case it was left open.
    if (ofile.is_open()) {
      ofile.close();
    }
    ofile.open(filename);

    if (!ofile) {
      std::cerr << "Could not open " << filename << "\n";
      return -1;
    }
    ofile << contents;
    ofile.close();
  }
  return 0;
}

/**
 * @brief Map the elve cut patterns to the santa cloth
 * @param map
 * @param buffer
 */
void build(const Data& map, std::vector<std::vector<Vec3>>& buffer)
{
  Vec3 color(0, 0, 0);
  for (int y = map.pos.y(); y <= map.pos.y() + map.area.y(); y++) {
    for (int x = map.pos.x(); x <= (map.pos.x() + map.area.x()); x++) {
      buffer[x][y] = map.color;
    }
  }
}

/**
 * @brief Render the buffer into a stringstream formated for the ppm image format.
 * @param res
 * @param samples
 * @return
 */
std::stringstream renderPPM(Data& cloth, std::vector<std::vector<Vec3>>& buffer)
{
  std::stringstream buf;
  buf << "P3\n" << cloth.area.x() << " " << cloth.area.y() << "\n255\n";

  for (int j = cloth.pos.y(); j <= cloth.pos.y() + cloth.area.y(); j++) {
    for (int i = cloth.pos.x(); i < cloth.pos.x() + cloth.area.x(); i++) {
      Vec3 color = buffer[i][j];
      color      = Vec3(sqrt(color[0]), sqrt(color[1]), sqrt(color[2]));
      int ir     = int(255.99 * color[0]);
      int ig     = int(255.99 * color[1]);
      int ib     = int(255.99 * color[2]);
      buf << ir << " " << ig << " " << ib << "\n";
    }
  }
  return buf;
}

/**
 * @brief Read the input file from disk
 * @param name
 * @return
 */
std::vector<Data> read_file(const string& name)
{
  std::vector<Data> list;

  std::ifstream file(name, std::ifstream::in);
  if (!file.is_open()) {
    throw runtime_error("Failed to open file for read!");
  }

  string id;
  string xy;
  string wh;
  string dump;

  char c = ' ';
  while (file >> id >> dump >> xy >> wh) {
    Data l;
    l.id = id;

    stringstream ss(xy);
    string       buffer;

    while ((c = ss.get()) != ',') {
      buffer += c;
    }
    stringstream ssbuffer(buffer);
    ssbuffer >> l.pos.e[0];

    buffer.clear();
    while ((c = ss.get()) != ':') {
      buffer += c;
    }
    ssbuffer = stringstream(buffer);
    ssbuffer >> l.pos.e[1];

    buffer.clear();
    ss = stringstream(wh);
    while ((c = ss.get()) != 'x') {
      buffer += c;
    }
    ssbuffer = stringstream(buffer);
    ssbuffer >> l.area.e[0];

    ss >> l.area.e[1];

    list.push_back(l);
  }

  file.close();
  return list;
}

/* Example inputs
 * #1 @ 527,351: 24x10
 * #1037 @ 84,89: 19x17
 */

int main(int argc, char* argv[])
{
  // The cloth the elves want to cut
  Data                           cloth{"cloth", {0, 0}, {1000, 1000}, {1, 0, 0}};
  /* I used #1 as a test */ Data test{"#1", {527, 351}, {24, 10}, {0, 1, 0}};

  // Create a buffer and fill the color with the santa cloth color
  auto buffer = std::vector<std::vector<Vec3>>(1000, vector<Vec3>(1000));
  Vec3 color  = Vec3(1, 0, 0);
  for (int i = 0; i < 1000; i++) {
    for (int j = 0; j < 1000; j++) {
      buffer[i][j] = cloth.color;
    }
  }

  // Read in the input data from file
  vector<Data> list = read_file("input.txt");

  // Generate a color table, would be better to dump this to file, and load it when I need it later.
  vector<Vec3> colors;
  for (float i = 0.0; i < 1; i += 0.01) {
    for (float j = 0.0; j < 1; j += 0.01) {
      for (float k = 0.0; k < 1; k += 0.01) {
        colors.push_back(Vec3(i, j, k));
      }
    }
  }

  // Set the different colors for the design patterens with colors vector that was previously generated.
  int color_count             = colors.size();
  int list_count              = list.size();
  int color_to_list_increment = color_count / list_count - 250;
  int color_to_list_count     = 0;
  for (int i = 0; i <= list.size(); i++) {
    list[i].color = colors[i + color_to_list_count];
    color_to_list_count += color_to_list_increment;
  }

  // Write the different patterns to the main buffer in an overlapping pattern.
  for (const auto& l : list) {
    build(l, buffer);
  }

  // Generate the buffer into a stringstream and then pass it to writeFilePPM as a string.
  stringstream image = renderPPM(cloth, buffer);
  writeFilePPM("test.ppm", image.str());

  return EXIT_SUCCESS;
}
